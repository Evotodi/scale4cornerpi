# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'base.ui'
#
# Created: Sat Jan  7 00:08:17 2017
#      by: PyQt4 UI code generator 4.11.2
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_BaseWindow(object):
    def setupUi(self, BaseWindow):
        BaseWindow.setObjectName(_fromUtf8("BaseWindow"))
        BaseWindow.resize(800, 480)
        BaseWindow.setWindowTitle(_fromUtf8("BaseWindow"))
        BaseWindow.setStyleSheet(_fromUtf8("QMainWindow { background: white }"))
        self.centralwidget = QtGui.QWidget(BaseWindow)
        self.centralwidget.setAutoFillBackground(False)
        self.centralwidget.setStyleSheet(_fromUtf8("QWidget { background-image: url(:/images/bg1.jpg) }"))
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        BaseWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(BaseWindow)
        QtCore.QMetaObject.connectSlotsByName(BaseWindow)

    def retranslateUi(self, BaseWindow):
        pass

import resrepo_rc
