#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import getopt
import os
import statistics
import sys
import threading
import time

import RPi.GPIO as GPIO
import yaml
from PyQt4 import QtGui, QtCore

import base
import numpad
import page1
import page2
import page3
import setup
from hx711 import HX711
from scale import Scale

import pymsgbox
import psutil
import resrepo_rc


class Base(QtGui.QMainWindow, base.Ui_BaseWindow):
    def __init__(self):
        super(self.__class__, self).__init__()
        self.setupUi(self)

    def closeEvent(self, event):
        print("CLOSE EVENT")
        try:
            readings_t.do_run = False
            readings_t.join()
        finally:
            self.close()


class Page1(QtGui.QMainWindow, page1.Ui_Page1Window):
    def __init__(self):
        super(self.__class__, self).__init__()
        self.setupUi(self)
        self.btnPage2.clicked.connect(self.page2_show)
        self.btnPage3.clicked.connect(self.page3_show)
        self.btnSetup.clicked.connect(self.setup_show)

    @staticmethod
    def page2_show():
        print("PAGE 2 SHOW")
        pg1.close()
        if debug:
            pg2.show()
        else:
            pg2.setWindowFlags(QtCore.Qt.FramelessWindowHint)
            pg2.move(0, 0)
            pg2.showFullScreen()

    @staticmethod
    def page3_show():
        print("PAGE 2 SHOW")
        pg1.close()
        if debug:
            pg3.show()
        else:
            pg3.setWindowFlags(QtCore.Qt.FramelessWindowHint)
            pg3.move(0, 0)
            pg3.showFullScreen()

    @staticmethod
    def setup_show():
        print("PAGE SETUP SHOW")
        pg1.close()
        if debug:
            setuppg.show()
        else:
            setuppg.setWindowFlags(QtCore.Qt.FramelessWindowHint)
            setuppg.move(0, 0)
            setuppg.showFullScreen()


class Page2(QtGui.QMainWindow, page2.Ui_Page2Window):
    def __init__(self):
        super(self.__class__, self).__init__()
        self.setupUi(self)
        self.btnPage1.clicked.connect(self.page1_show)
        self.btnPage3.clicked.connect(self.page3_show)
        self.btnSetup.clicked.connect(self.setup_show)

    @staticmethod
    def page1_show():
        print("PAGE 1 SHOW")
        pg2.close()
        if debug:
            pg1.show()
        else:
            pg1.setWindowFlags(QtCore.Qt.FramelessWindowHint)
            pg1.move(0, 0)
            pg1.showFullScreen()

    @staticmethod
    def page3_show():
        print("PAGE 1 SHOW")
        pg2.close()
        if debug:
            pg3.show()
        else:
            pg3.setWindowFlags(QtCore.Qt.FramelessWindowHint)
            pg3.move(0, 0)
            pg3.showFullScreen()

    @staticmethod
    def setup_show():
        print("PAGE SETUP SHOW")
        pg2.close()
        if debug:
            setuppg.show()
        else:
            setuppg.setWindowFlags(QtCore.Qt.FramelessWindowHint)
            setuppg.move(0, 0)
            setuppg.showFullScreen()


class Page3(QtGui.QMainWindow, page3.Ui_Page3Window):
    def __init__(self):
        super(self.__class__, self).__init__()
        self.setupUi(self)
        self.btnPage1.clicked.connect(self.page1_show)
        self.btnPage2.clicked.connect(self.page2_show)
        self.btnSetup.clicked.connect(self.setup_show)

    @staticmethod
    def page1_show():
        print("PAGE 1 SHOW")
        pg3.close()
        if debug:
            pg1.show()
        else:
            pg1.setWindowFlags(QtCore.Qt.FramelessWindowHint)
            pg1.move(0, 0)
            pg1.showFullScreen()

    @staticmethod
    def page2_show():
        print("PAGE 2 SHOW")
        pg3.close()
        if debug:
            pg2.show()
        else:
            pg2.setWindowFlags(QtCore.Qt.FramelessWindowHint)
            pg2.move(0, 0)
            pg2.showFullScreen()

    @staticmethod
    def setup_show():
        print("PAGE SETUP SHOW")
        pg3.close()
        if debug:
            setuppg.show()
        else:
            setuppg.setWindowFlags(QtCore.Qt.FramelessWindowHint)
            setuppg.move(0, 0)
            setuppg.showFullScreen()


class PageSetup(QtGui.QMainWindow, setup.Ui_PageSetupWindow):
    cells = {}

    def __init__(self):
        super(self.__class__, self).__init__()
        self.setupUi(self)
        self.btnPage1.clicked.connect(self.page1_show)
        self.btnPage2.clicked.connect(self.page2_show)
        self.btnPage3.clicked.connect(self.page3_show)
        self.btnHalt.clicked.connect(self.halt)
        self.btnTare.clicked.connect(self.tare_scale)
        self.btnLFEdit.clicked.connect(lambda: self.editval("lf"))
        self.btnRFEdit.clicked.connect(lambda: self.editval("rf"))
        self.btnLREdit.clicked.connect(lambda: self.editval("lr"))
        self.btnRREdit.clicked.connect(lambda: self.editval("rr"))
        self.btnSave.clicked.connect(self.save_vals)
        self.init_cells()

    @staticmethod
    def page1_show(pargs):
        print("PAGE 1 SHOW")
        setuppg.close()
        if debug:
            pg1.show()
        else:
            pg1.setWindowFlags(QtCore.Qt.FramelessWindowHint)
            pg1.move(0, 0)
            pg1.showFullScreen()
            if pargs == "tare":
                pymsgbox.alert(text='Tare Complete', title='Alert', button='OK')

    @staticmethod
    def page2_show():
        print("PAGE 2 SHOW")
        setuppg.close()
        if debug:
            pg2.show()
        else:
            pg2.setWindowFlags(QtCore.Qt.FramelessWindowHint)
            pg2.move(0, 0)
            pg2.showFullScreen()

    @staticmethod
    def page3_show():
        print("PAGE 3 SHOW")
        setuppg.close()
        if debug:
            pg3.show()
        else:
            pg3.setWindowFlags(QtCore.Qt.FramelessWindowHint)
            pg3.move(0, 0)
            pg3.showFullScreen()

    def editval(self, cell):
        numpad = Numpad()
        numpad.move(20, 120)
        numpad.show()

        if cell == "lf":
            numpad.btnSave.clicked.connect(lambda: self.setval(self.LFscale, numpad.lineEdit.text(), numpad))
        elif cell == "rf":
            numpad.btnSave.clicked.connect(lambda: self.setval(self.RFscale, numpad.lineEdit.text(), numpad))
        elif cell == "lr":
            numpad.btnSave.clicked.connect(lambda: self.setval(self.LRscale, numpad.lineEdit.text(), numpad))
        elif cell == "rr":
            numpad.btnSave.clicked.connect(lambda: self.setval(self.RRscale, numpad.lineEdit.text(), numpad))

    @staticmethod
    def setval(widget, val, dialog):
        widget.setValue(float(val))
        dialog.close()

    def save_vals(self):
        global settings
        settings['lfscale'] = self.LFscale.value()
        settings['rfscale'] = self.RFscale.value()
        settings['lrscale'] = self.LRscale.value()
        settings['rrscale'] = self.RRscale.value()
        save_settings()

    def init_cells(self):
        global settings
        self.LFscale.setValue(float(settings['lfscale']))
        self.RFscale.setValue(float(settings['rfscale']))
        self.LRscale.setValue(float(settings['lrscale']))
        self.RRscale.setValue(float(settings['rrscale']))

    @staticmethod
    def halt():
        response = pymsgbox.confirm('Shutdown CornerScale?', 'Confirm Shutdown', ["Yes, I'm sure.", 'Cancel'])
        if response == "Yes, I'm sure.":
            os.system("sudo shutdown -h -P now")  # shuts down Pi

    def tare_scale(self):
        response = pymsgbox.confirm('Tare the scale?', 'Confirm Tare', ["Yes, I'm sure.", 'Cancel'])
        if response == "Yes, I'm sure.":
            readings_t.tare = True
            self.page1_show("tare")


class Numpad(QtGui.QMainWindow, numpad.Ui_Numpad):
    def __init__(self):
        super(self.__class__, self).__init__()
        self.setupUi(self)
        self.btn0.clicked.connect(lambda: self.btn_click(0))
        self.btn1.clicked.connect(lambda: self.btn_click(1))
        self.btn2.clicked.connect(lambda: self.btn_click(2))
        self.btn3.clicked.connect(lambda: self.btn_click(3))
        self.btn4.clicked.connect(lambda: self.btn_click(4))
        self.btn5.clicked.connect(lambda: self.btn_click(5))
        self.btn6.clicked.connect(lambda: self.btn_click(6))
        self.btn7.clicked.connect(lambda: self.btn_click(7))
        self.btn8.clicked.connect(lambda: self.btn_click(8))
        self.btn9.clicked.connect(lambda: self.btn_click(9))
        self.btnPoint.clicked.connect(lambda: self.btn_click(99))
        self.btnBack.clicked.connect(lambda: self.btn_click(-1))

    def btn_click(self, val):
        if 0 <= val <= 9:
            t = self.lineEdit.text()
            t += str(val)
            self.lineEdit.setText(t)
        elif val == 99:
            t = self.lineEdit.text()
            t += "."
            self.lineEdit.setText(t)
        else:
            t = self.lineEdit.text()
            t = t[:-1]
            self.lineEdit.setText(t)


def readings(rarg):
    global LF, RF, LR, RR, leftTotal, leftPercent, rightTotal, rightPercent, frontTotal, frontPercent, rearTotal, rearPercent
    global crossTotal, crossPercent, wedgeTotal, wedgePercent, wedgeDiff, LFRRTotal, LFRRPercent, total, bite
    global pg1, pg2, pg3, settings, debug, GetReadingCount, HistoryLength

    # These are debug only
    LF = 650.111
    RF = 730.222
    LR = 714.333
    RR = 615.444
    # ####################

    hx_lf = HX711(dout=17, pd_sck=18)
    scale_lf = Scale(hx_lf, 10, 100, 0.1)
    scale_lf.setOffset(settings['lfoffset'])
    scale_lf.setReferenceUnit(settings['lfscale'])
    scale_lf.reset()
    # scale_lf.tare()
    history_lf = []

    hx_rf = HX711(dout=9, pd_sck=25)
    scale_rf = Scale(hx_rf, 10, 100, 0.1)
    scale_rf.setOffset(settings['rfoffset'])
    scale_rf.setReferenceUnit(settings['rfscale'])
    scale_rf.reset()
    # scale_rf.tare()
    history_rf = []

    hx_lr = HX711(dout=22, pd_sck=23)
    scale_lr = Scale(hx_lr, 10, 100, 0.1)
    scale_lr.setOffset(settings['lroffset'])
    scale_lr.setReferenceUnit(settings['lrscale'])
    scale_lr.reset()
    # scale_lr.tare()
    history_lr = []

    hx_rr = HX711(dout=11, pd_sck=8)
    scale_rr = Scale(hx_rr, 10, 100, 0.1)
    scale_rr.setOffset(settings['rroffset'])
    scale_rr.setReferenceUnit(settings['rrscale'])
    scale_rr.reset()
    # scale_rr.tare()
    history_rr = []

    t = threading.currentThread()
    while getattr(t, "do_run", True):
        if debug:
            print("working on %s" % rarg)

        try:

            if getattr(t, "tare"):
                scale_lf.reset()
                scale_lf.tare()
                settings['lfoffset'] = scale_lf.getOffset()
                scale_rf.reset()
                scale_rf.tare()
                settings['rfoffset'] = scale_rf.getOffset()
                scale_lr.reset()
                scale_lr.tare()
                settings['lroffset'] = scale_lr.getOffset()
                scale_rr.reset()
                scale_rr.tare()
                settings['rroffset'] = scale_rr.getOffset()
                save_settings()
                setattr(t, "tare", False)

            if getattr(t, "offset"):
                scale_lf.setReferenceUnit(settings['lfscale'])
                scale_lf.reset()
                scale_rf.setReferenceUnit(settings['rfscale'])
                scale_rf.reset()
                scale_lr.setReferenceUnit(settings['lrscale'])
                scale_lr.reset()
                scale_rr.setReferenceUnit(settings['rrscale'])
                scale_rr.reset()
                setattr(t, "offset", False)
                time.sleep(0.5)

            val = scale_lf.getWeight(GetReadingCount)
            if val < 0.01 and not debug:
                val = 0.001
            history_lf.append(val)
            history_lf = history_lf[-HistoryLength:]
            md = statistics.median(history_lf)
            LF = to_lbs(md)
            if debug:
                print("LF lbs = {0: 4.4f}".format(LF))

            val = scale_rf.getWeight(GetReadingCount)
            if val < 0.01 and not debug:
                val = 0.001
            history_rf.append(val)
            history_rf = history_rf[-HistoryLength:]
            md = statistics.median(history_rf)
            RF = to_lbs(md)
            if debug:
                print("RF lbs = {0: 4.4f}".format(RF))

            val = scale_lr.getWeight(GetReadingCount)
            if val < 0.01 and not debug:
                val = 0.001
            history_lr.append(val)
            history_lr = history_lr[-HistoryLength:]
            md = statistics.median(history_lr)
            LR = to_lbs(md)
            if debug:
                print("LR lbs = {0: 4.4f}".format(LR))

            val = scale_rr.getWeight(GetReadingCount)
            if val < 0.01 and not debug:
                val = 0.001
            history_rr.append(val)
            history_rr = history_rr[-HistoryLength:]
            md = statistics.median(history_rr)
            RR = to_lbs(md)
            if debug:
                print("RR lbs = {0: 4.4f}".format(RR))

            total = LF + RF + LR + RR
            leftTotal = LF + LR
            leftPercent = (leftTotal / total) * 100.0
            rightTotal = RF + RR
            rightPercent = (rightTotal / total) * 100.0
            frontTotal = LF + RF
            frontPercent = (frontTotal / total) * 100.0
            rearTotal = LR + RR
            rearPercent = (rearTotal / total) * 100.0
            crossTotal = RF + LR
            crossPercent = ((RF + LR) / total) * 100.0
            LFRRTotal = LF + RR
            LFRRPercent = (LFRRTotal / total) * 100.0
            wedgeTotal = crossTotal
            wedgePercent = crossPercent
            wedgeDiff = LFRRTotal - wedgeTotal
            bite = LR - RR

            pg1.LF.display("{0: 4.2f}".format(round(LF, 2)))
            pg1.RF.display("{0: 4.2f}".format(round(RF, 2)))
            pg1.LR.display("{0: 4.2f}".format(round(LR, 2)))
            pg1.RR.display("{0: 4.2f}".format(round(RR, 2)))
            pg1.FrontTotal.display("{0: 4.2f}".format(round(frontTotal, 2)))
            pg1.FrontPercent.display("{0: 4.2f}".format(round(frontPercent, 2)))
            pg1.RightTotal.display("{0: 4.2f}".format(round(rightTotal, 2)))
            pg1.RightPercent.display("{0: 4.2f}".format(round(rightPercent, 2)))
            pg1.RearTotal.display("{0: 4.2f}".format(round(rearTotal, 2)))
            pg1.RearPercent.display("{0: 4.2f}".format(round(rearPercent, 2)))
            pg1.LeftTotal.display("{0: 4.2f}".format(round(leftTotal, 2)))
            pg1.LeftPercent.display("{0: 4.2f}".format(round(leftPercent, 2)))
            pg1.CenterTotal.display("{0: 4.2f}".format(round(crossTotal, 2)))
            pg1.CenterPercent.display("{0: 4.2f}".format(round(crossPercent, 2)))

            pg2.total.display("{0: 4.2f}".format(round(total, 2)))
            pg2.wedgeTotal.display("{0: 4.2f}".format(round(wedgeTotal, 2)))
            pg2.wedgePercent.display("{0: 4.2f}".format(round(wedgePercent, 2)))
            pg2.wedgeDiff.display("{0: 4.2f}".format(round(wedgeDiff, 2)))
            pg2.wedgePercent.display("{0: 4.2f}".format(round(wedgePercent, 2)))
            pg2.LFRRTotal.display("{0: 4.2f}".format(round(LFRRTotal, 2)))
            pg2.LFRRPercent.display("{0: 4.2f}".format(round(LFRRPercent, 2)))

            LFTarget = leftTotal * (frontPercent * 0.01)
            RFTarget = rightTotal * (frontPercent * 0.01)
            LRTarget = leftTotal * (rearPercent * 0.01)
            RRTarget = rightTotal * (rearPercent * 0.01)
            LFDiff = LFTarget - LF
            RFDiff = RFTarget - RF
            LRDiff = LRTarget - LR
            RRDiff = RRTarget - RR

            pg3.LF.display("{0: 4.2f}".format(round(LF, 2)))
            pg3.LFTarget.display("{0: 4.2f}".format(round(LFTarget, 2)))
            pg3.LFDiff.display("{0: 4.2f}".format(round(LFDiff, 2)))
            pg3.RF.display("{0: 4.2f}".format(round(RF, 2)))
            pg3.RFTarget.display("{0: 4.2f}".format(round(RFTarget, 2)))
            pg3.RFDiff.display("{0: 4.2f}".format(round(RFDiff, 2)))
            pg3.LR.display("{0: 4.2f}".format(round(LR, 2)))
            pg3.LRTarget.display("{0: 4.2f}".format(round(LRTarget, 2)))
            pg3.LRDiff.display("{0: 4.2f}".format(round(LRDiff, 2)))
            pg3.RR.display("{0: 4.2f}".format(round(RR, 2)))
            pg3.RRTarget.display("{0: 4.2f}".format(round(RRTarget, 2)))
            pg3.RRDiff.display("{0: 4.2f}".format(round(RRDiff, 2)))

            if debug:
                time.sleep(1)
            time.sleep(0.5)

        except ZeroDivisionError:
            if debug:
                print("Zero Division")
    if debug:
        print("Stopping as you wish")
    GPIO.cleanup()


def to_lbs(grams):
    return round(grams * 0.00220462262, 4)


def load_settings():
    global settings, debug
    fs = open('settings.yaml', 'r')
    settings = yaml.load(fs)
    fs.close()
    if debug:
        print(yaml.dump(settings, default_flow_style=False))


def save_settings():
    global settings, debug, readings_t
    fs = open('settings.yaml', 'w')
    fs.write(yaml.dump(settings, default_flow_style=False))
    fs.close()
    readings_t.offset = True
    if debug:
        print(yaml.dump(settings, default_flow_style=False))


def main():
    global app, basepg, pg1

    if debug:
        basepg.show()
    else:
        basepg.setWindowFlags(QtCore.Qt.FramelessWindowHint)
        basepg.move(0, 0)
        basepg.showFullScreen()

    app.processEvents()

    if debug:
        time.sleep(1)
        pg1.show()
    else:
        time.sleep(7)
        pg1.setWindowFlags(QtCore.Qt.FramelessWindowHint)
        pg1.move(0, 0)
        pg1.showFullScreen()

    readings_t.start()
    sys.exit(app.exec_())


if __name__ == '__main__':
    # Variables
    debug = False
    LF = 0.0
    RF = 0.0
    LR = 0.0
    RR = 0.0
    leftTotal = 0.0
    leftPercent = 0.0
    rightTotal = 0.0
    rightPercent = 0.0
    frontTotal = 0.0
    frontPercent = 0.0
    rearTotal = 0.0
    rearPercent = 0.0
    crossTotal = 0.0
    crossPercent = 0.0
    wedgeTotal = 0.0
    wedgePercent = 0.0
    wedgeDiff = 0.0
    LFRRTotal = 0.0
    LFRRPercent = 0.0
    total = 0.0
    bite = 0.0
    # Variables

    # Constants
    GetReadingCount = 3
    HistoryLength = 5
    # Constants

    try:
        opts, args = getopt.getopt(sys.argv[1:], "d", ["debug"])
    except getopt.GetoptError:
        print('test.py (-d --debug)')
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-d", "--debug"):
            debug = True
            print("DEBUG ENABLED")

    settings = {}
    load_settings()

    app = QtGui.QApplication(sys.argv)
    basepg = Base()
    setuppg = PageSetup()
    pg1 = Page1()
    pg2 = Page2()
    pg3 = Page3()

    readings_t = threading.Thread(target=readings, args=("task",))
    readings_t.tare = False
    readings_t.offset = False

    main()

